import React, { useState, useEffect, Suspense } from "react";
import "./App.css";


const LineChart = React.lazy(() => import("./components/LineChart"))

const App = () => {
	const [data, setData] = useState([]);

	useEffect(() => {
		regenerateData();
	}, []);

	function regenerateData() {
		const chartData = [];
		for (let i = 0; i < 40; i++) {
			const value = Math.floor(Math.random() * i + 3);
			chartData.push({
				label: i,
				value,
				tooltipContent: `<b>x: </b>${i}<br><b>y: </b>${value}`,
			});
		}
		setData(chartData);
	}
	return (
		<div className="App">
			<button onClick={regenerateData}>Change Data</button>
			<Suspense fallback="Loading...">
				<LineChart
					svgProps={{
						margin: { top: 80, bottom: 80, left: 80, right: 80 },
						height: 400,
						width: 800,
					}}
					axisProps={{
						xLabel: "X Axis",
						yLabel: "Y Axis",
					}}
					data={data}
					strokeWidth={4}
				/>
			</Suspense>
			
		</div>
	);
};

export default App;
